import java.util.HashMap;
import java.util.Map;

import spark.ModelAndView;
import spark.template.handlebars.HandlebarsTemplateEngine;

import static spark.Spark.*;

public class App {

    public static void main(String[] args) {
        staticFileLocation("/public");
        //this is how u add routes
        get("/", (request, response) -> { //request for route happens at this location
            Map<String, Object> model = new HashMap<String, Object>(); // new model is made to store information
            return new ModelAndView(model, "hello.hbs"); // assemble individual pieces and render
        }, new HandlebarsTemplateEngine());

        get("/favorite_photos", (request, response) -> { //request for route happens at this location
            Map<String, Object> model = new HashMap<String, Object>(); // new model is made to store information
            return new ModelAndView(model, "favorite_photos.hbs"); // assemble individual pieces and render
        }, new HandlebarsTemplateEngine());

        get("/forms", (request, response) -> { //request for route happens at this location
            Map<String, Object> model = new HashMap<String, Object>(); // new model is made to store information
            return new ModelAndView(model, "forms.hbs"); // assemble individual pieces and render
        }, new HandlebarsTemplateEngine());
// submitting using get
        get("/greeting_card", (request, response) -> {
            Map<String, Object> model = new HashMap<String, Object>();
            String recipient = request.queryParams("recipient"); //query params retrieves users input
            String sender = request.queryParams("sender");
            model.put("recipient", recipient); //then we save input to the hashmap
            model.put("sender", sender);
            return new ModelAndView(model, "greeting_card.hbs"); //finally we return our new view with the data passed as info.
        }, new HandlebarsTemplateEngine());

        //submitting using post
        post("/greeting_card",(request, response) -> {
            Map<String, Object> model = new HashMap<String, Object>();
            //fetching users input
            String recipient = request.queryParams("recipient");
            String sender = request.queryParams("sender");
            //saving input to session
            request.session().attribute("recipient", recipient);
            request.session().attribute("sender", sender);
            //adding input to model
            model.put("recipient", recipient);
            model.put("sender", sender);
             //returning new view with info
            return new ModelAndView(model, "greeting_card.hbs");

        }, new HandlebarsTemplateEngine());
    }
}